# README #

###Public repo
  https://bitbucket.org/esafronoff/devchallenge-final-17/overview

### Description ###

* node.js version LTS(6.9.1)
* We host app on https://www.evennode.com 
* Domain name: devchallenge.eu-2.evennode.com
* Gite repo for deploy evennode: git@git.evennode.com:085c435a1d86eee02bb0f073567e50c4.git

###External
    Aplivaation availiable by link http://devchallenge.eu-2.evennode.com/
    

### Setting rules ###

* node.js version LTS(6.9.1)
* angular 1.5.8
* json-server as data storage

#### [Backend]
* in root folder: npm install
* PORT=5001 node app.js

Then local server should starts on 5001 port: http://localhost:5001
API: http://localhost:5001/api/

### [frontend]
* move to angular-app folder cd public
* npm install 
* npm install -g gulp
* gulp

Then application should work by url: http://localhost:5001

###ENDPOINTS

* USERS

    {DOMAIN}/api/users
    Methods: GET, POST, PUT, DELETE
 
* STREAMS
 
    {DOMAIN}/api/streams
    Methods: GET, POST, PUT, DELETE
 
* MESSAGES
 
    {DOMAIN}/api/messages
    Methods: GET, POST, PUT, DELETE
 
* EVENTS  
 
    {DOMAIN}/api/events
    Methods: GET, POST, PUT, DELETE
 
 * AGGREGATE_STEAMS
 
    {DOMAIN}/api/api/streams/aggregate/all
    Methods: GET
