'use strict';

const PORT = process.env.PORT || 5001;
const DB_NAME = 'db.json';

const _ = require('lodash');
const lowdb = require('lowdb');

const jsonServer = require('json-server');
const router = jsonServer.router(DB_NAME);
const middlewares = jsonServer.defaults()
const server = jsonServer.create();

//init constant-helpers
const API = '/api';
const USERS_URL = `${API}/users`;
const STREAMS_URL = `${API}/streams`;
const STREAM_AGGREGATE_ALL_URL = `${API}/streams/aggregate/all`;

server.use(middlewares);

server.use(jsonServer.bodyParser);

server.use((req, res, next) => {
    //redirect each request to start point if  it is not api request
    if(!req.url.match(/^\/api/gi)) {
        res.redirect('/');
    };

    if (req.method === 'POST') {
        req.body.createdAt = Date.now();
        //handle user POST request
        if(req.url === USERS_URL) {
            const db = lowdb(DB_NAME);
            const userEmail = _.get(req, 'body.email');
            const userPassword = _.get(req, 'body.password');
            const firstUser = db.get('users').find({ email: userEmail }).value();

            if(firstUser && userPassword === _.get(firstUser, 'password')){
                console.log(`User sent correct email and password. Login.`);
                res.status(200).json(firstUser);
            }

            if (firstUser) {
                console.log(`User with this email: ${userEmail} already exist you provide wrong password!`);
                res.status(409).json({ message : `User with this email: ${userEmail} already exist!` });
            }
        }

        //handle stream POST request
        if(req.url === STREAMS_URL) {
            const db = lowdb(DB_NAME);
            const users = db.get('users').value();

            const allowUserIds = _.get(req.body, 'allowUserIds');
            const allowUsers = _.map(allowUserIds, uid => {
                const user = _.find(users, { id : uid});
                return { name: _.get(user, 'name'), email: _.get(user, 'email'), id: _.get(user, 'id')}
            })
            _.merge(req.body, { allowUsers })
        }
    }

    //handle STREAM_AGGREGATE_ALL_URL GET request
    if (req.method === 'GET') {
        if(req.url === STREAM_AGGREGATE_ALL_URL) {
            const db = lowdb(DB_NAME);
            //get all records
            const streams = db.get('streams').value();
            const users = db.get('users').value();
            const events = db.get('events').value();

            _.forEach(events, event => {
                const participatorIds = _.get(event, 'participatorIds');
                const participators = _.map(participatorIds, pid => {
                    const user = _.find(users, { id : pid});
                    return { name: _.get(user, 'name'), email : _.get(user, 'email'), id: pid}
                });
                _.merge(event, { participators });
            });

            const messages = db.get('messages').value();
            const records = _.concat(messages, events);


            _.forEach(records, record => {
                const userId = _.get(record, 'userId');
                const user = _.find(users, { id : userId });
                _.merge(record, {
                    author: { name : _.get(user, 'name'), email : _.get(user, 'email')}
                });
            })

            _.forEach(streams, stream => {
                const streamId = _.get(stream, 'id');
                const allowUserIds = _.get(stream, 'allowUserIds');

                const allowUsers = _.map(allowUserIds, uid => {
                    const user = _.find(users, { id : uid});
                    return { name : _.get(user, 'name'), email : _.get(user, 'email')}
                })

                _.merge(stream, {
                    records: _.filter(records, { streamId : streamId }),
                    allowUsers
                });
            });
            res.status(200).json(streams);
            return;
        }
    }
    next();
});

server.use(API, router);

server.listen(PORT, () => {
    console.log(`Application listening on port ${PORT}!`);
})