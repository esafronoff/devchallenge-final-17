class CreateEventController {
    constructor(streamService, $mdDialog, userService, $rootScope) {
        this.event = {
            title: '',
            date: '',
            price: '',
            url: '',
            description: '',
            userId: userService.getCurrentUser().id,
            streamId: this.stream.id
        };
        this.streamService = streamService;
        this.$mdDialog = $mdDialog;
        this.createEventError = null;
        this.$rootScope = $rootScope;
    }

    createEvent(event) {
        this.createEventError = null;
        let data = angular.copy(event);
        data.date = +new Date(data.date);
        this.streamService
            .sendEvent(data)
            .then((resp) => {
                this.$mdDialog.hide();
                this.$rootScope.$emit(`stream-new-message`, resp.data);
            })
            .catch(this.createEventError = 'Event creation error');
    }
}

angular.module('modules.components').component('createEventForm', {
    template: `<div ng-cloak class="create-event-from">
                    <h1>Create Event</h1>
                    <div>
                        <form name="createEventForm">
                        
                            <div class="create-event-error" 
                                 ng-if="vm.createEventError" 
                                 ng-bind="vm.createEventError"></div>
                                 
                              <md-input-container class="md-block">
                                <label>Title</label>
                                <input ng-model="vm.event.title" name="title" type="text" required>
                              </md-input-container>
                                                              
                              <div layout="row">
                                  <div flex="50">
                                   <md-input-container class="md-block">
                                        <label>Date</label>
                                        <md-datepicker ng-model="vm.event.date" 
                                                       md-placeholder="Enter event date" 
                                                       name="date" 
                                                       required></md-datepicker>
                                    </md-input-container>
                                  </div>
                                  <div flex="50">
                                      <md-input-container class="md-block">
                                        <label>Price, $</label>
                                        <input ng-model="vm.event.price" name="price" type="number" required>
                                      </md-input-container>
                                  </div>
                              </div>
                              
                              <md-input-container class="md-block">
                                <label>Image URL</label>
                                <input ng-model="vm.event.url" name="url" type="text" required>
                              </md-input-container>
                              
                              <md-input-container class="md-block">
                                <label>Description</label>
                                <textarea ng-model="vm.event.description" name="description"></textarea>
                              </md-input-container>
                              
                              <md-button class="md-raised md-primary" 
                                         ng-click="vm.createEvent(vm.event)" 
                                         ng-disabled="createEventForm.$invalid">Create event</md-button>
                                         
                        </form>
                    </div>
                </div>`,
    controller: CreateEventController,
    controllerAs: 'vm',
    bindings: {
        stream: '='
    }
});
