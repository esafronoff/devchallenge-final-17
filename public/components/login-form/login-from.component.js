class LoginFormController {
    constructor(userService,$state) {
        this.user = {
            email: '',
            password: '',
            name: ''
        };
        this.userService = userService;
        this.signInError = null;
        this.$state = $state;
    }
    
    signIn(user) {
        this.signInError = null;
        user.name = user.email;
        this.userService
            .signIn(user)
            .then(user => this.$state.go('stream'))
            .catch(error => this.signInError = error);
    }
}

angular.module('modules.components').component('loginForm', {
    template: `<div ng-cloak class="login-from">
                    <h1>LOGIN USER</h1>
                    <div>
                        <form name="signInForm">
                            <div class="sign-in-error" ng-if="vm.signInError" ng-bind="vm.signInError"></div>
                              <md-input-container class="md-block">
                                <label>Email</label>
                                <input ng-model="vm.user.email" name="email" type="email" required>
                              </md-input-container>
                              <md-input-container class="md-block"> 
                                <label>Password</label>
                                <input ng-model="vm.user.password" name="password" type="password" required>
                              </md-input-container>
                            <md-button class="md-raised md-primary" ng-click="vm.signIn(vm.user)" ng-disabled="signInForm.$invalid">Login</md-button>
                            <div class="login-info">At first login, new user will be created</div>
                        </form>
                    </div>
                </div>`,
    controller: LoginFormController,
    controllerAs: 'vm'
});
