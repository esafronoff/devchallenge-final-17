class CreateStream {
    constructor(streamService,userService,$mdDialog,$rootScope) {
        this.stream = {
            title: '',
            description: '',
            isPrivate: false,
            users:[]
        };
        this.users = [];
        this.selected = [];

        this.streamService = streamService;
        this.userService = userService;
        this.$mdDialog = $mdDialog;
        this.$rootScope = $rootScope;
        this.currentUser = this.userService.getCurrentUser();

        this.userService.getUsers()
            .then(users => {
                this.users = users;
            });
    }


    create(stream) {
        let data = {
                userId: this.currentUser.id,
                title:stream.title,
                description:stream.description,
                isPublic:!stream.isPrivate,
                allowUserIds:[this.currentUser.id]
            };

        angular.forEach(this.selected,function(v){
            data.allowUserIds.push(v.id);
        });

        this.streamService
            .newStream(data)
            .then(response => {
                this.$mdDialog.hide();
                this.$rootScope.addStream(response.data);
            });
    };

    cancel() {
        this.$mdDialog.cancel();
    };

    toggle(item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            list.push(item);
        }
    };

    exists(item, list) {
        return list.indexOf(item) > -1;
    };

    isIndeterminate() {
        return (this.selected.length !== 0 &&
        this.selected.length !== this.users.length);
    };

    isChecked() {
        return this.selected.length === this.users.length;
    };

    toggleAll() {
        if (this.selected.length === this.users.length) {
            this.selected = [];
        } else if (this.selected.length === 0 || this.selected.length > 0) {
            this.selected = this.users.slice(0);
        }
    };
}

angular.module('modules.components').component('createStream', {
    template: `<div ng-cloak id="create-stream">
                    <h1>CREATE STREAM</h1>
                    <div>
                        <form name="stream">
                            <md-input-container class="md-block">
                                <label>Title</label>
                                <input ng-model="vm.stream.title" name="title" type="text" required>
                            </md-input-container>
                            <md-input-container class="md-block"> 
                                <label>Description</label>
                                <input ng-model="vm.stream.description" name="stream" type="text" required>
                            </md-input-container>
                            <md-switch ng-model="vm.stream.isPrivate" aria-label="private">
                                Make this stream private
                            </md-switch>
                            <fieldset ng-if="vm.stream.isPrivate" class="fieldset" >
                              <legend class="legend">Who will have access to this stream</legend>
                              <div layout="row" layout-wrap flex>
                              <div flex-xs flex="50">
                                <md-checkbox aria-label="Select All"
                                             ng-checked="vm.isChecked()"
                                             md-indeterminate="vm.isIndeterminate()"
                                             ng-click="vm.toggleAll()">
                                  <span ng-if="isChecked()">Un-</span>Select All
                                </md-checkbox>
                              </div>
                                <div class="select-all-checkboxes" flex="100" ng-repeat="user in vm.users | filter:{ id: '!'+vm.currentUser.id} track by $index">
                                  <md-checkbox ng-checked="vm.exists(user, vm.selected)" ng-click="vm.toggle(user, vm.selected)">
                                   {{ user.name }}
                                  </md-checkbox>
                                </div>
                              </div>
                            </fieldset>
                            
                            <div class="control">
                                <md-button class="md-raised" ng-click="vm.cancel()">Cancel</md-button>
                                <md-button class="md-raised md-primary" ng-click="vm.create(vm.stream)" ng-disabled="signInForm.$invalid">Create</md-button>
                            </div>
                   
                        </form>
                    </div>
                </div>`,
    controller: CreateStream,
    controllerAs: 'vm'
});
