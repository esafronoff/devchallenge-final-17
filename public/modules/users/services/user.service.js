class UserService {
    constructor($q, $http, AppConfig, $cookies) {
        this.$q = $q;
        this.$http = $http;
        this.$cookies = $cookies;
        this.AppConfig = AppConfig;

        this.currentUser = angular.fromJson(this.$cookies.get('current-user'));
    }

    setCurrentUser(user) {
        this.currentUser = user;
        this.$cookies.put('current-user', angular.toJson(this.currentUser));
    }

    getCurrentUser() {
        return this.currentUser;
    }

    signOut() {
        this.setCurrentUser(null);
    }

    signIn(user) {
        let deferred = this.$q.defer();

        let url = this.AppConfig.apiUrl + '/users';
        this.$http
            .post(url, user)
            .then(response => {
                this.setCurrentUser(response.data);
                deferred.resolve(this.getCurrentUser());
            })
            .catch(resp => {
                deferred.reject('user signin error');
            });

        return deferred.promise;
    }

    getUser(id){
        let deferred = this.$q.defer();

        let url = `${this.AppConfig.apiUrl}/users?id=${id}`;
        this.$http.get(url)
            .then(resp => deferred.resolve(resp.data[0]));

        return deferred.promise;
    }

    getUsers(){
        let deferred = this.$q.defer(),
            url = this.AppConfig.apiUrl + '/users';


        this.$http.get(url, {})
            .then(function (response) {
                console.info('Got %s users',response.data.length);
                deferred.resolve(response.data)
            },function(e){
                console.warn('error');
                console.log(e)
            });

        return deferred.promise;
    }
}

angular.module('modules.users').service('userService', UserService);