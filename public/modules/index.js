angular.module('modules.pages', []);
angular.module('modules.users', []);
angular.module('modules.components', []);

angular.module('modules', [
    'modules.pages',
    'modules.components',
    'modules.users'
]);