angular.module('modules.pages')
    .component('stream',{
        bindings:{
            stream:'='
        },
        controller:['$scope','streamService', '$mdDialog','userService','$rootScope',function($scope,streamService,$mdDialog,userService,$rootScope){
            var $ctrl = this;

            $ctrl.allUsers = [];

            userService.getUsers().then(response => {
                $ctrl.allUsers = response
            });

            $ctrl.sendMessage = function(){
                let currentUser = userService.getCurrentUser(),
                    data = {};
                data.streamId = $ctrl.stream.id;
                data.userId = currentUser.id;
                data.text = $scope.message;
                data.type = 'MESSAGE';
                streamService
                    .sendMessage(data)
                    .then(response => {
                        response.data.author = currentUser;
                        $rootScope.$emit('stream-new-message',response.data);
                        $scope.message='';
                        $scope.new.message.$touched = false;
                    });
            };

            $ctrl.createEvent = function(ev){
                $ctrl.openModal(ev, '<create-event-form stream="stream"></create-event-form>');
            };

            $ctrl.createStream = function(ev){
                $ctrl.openModal(ev, '<create-stream></create-stream>');
            };

            $ctrl.openModal = function(ev, template) {
                $mdDialog.show({
                    controller: ($scope) => {
                        $scope.stream = $ctrl.stream;
                    },
                    template: template,
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true
                });
            };

            $rootScope.$on(`stream-new-message`, (ev, message) => {
                if(message.streamId === $ctrl.stream.id) {
                    $ctrl.stream.records.push(message);
                    console.info($ctrl.stream.records)

                }
            });

        }],
        templateUrl:'./modules/pages/stream-list/_components/stream/stream.html'
    });