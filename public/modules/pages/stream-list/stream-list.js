angular.module('modules.pages')
    .component('streamListPage',{
        controller:['$scope','streamService','$stateParams','$rootScope', 'userService', '$state', function($scope,streamService,$stateParams,$rootScope, userService,$state){
            var $ctrl = this;

            $scope.$stateParams = $stateParams;

            $scope.userService = userService;


            $scope.logout = function() {
                userService.setCurrentUser(null);
                $state.go('home');
            };

            $ctrl.streams = [];

            streamService.get().then((data) => {
                $ctrl.streams = data.data || [];
            });

            $rootScope.addStream = function(stream){
                $ctrl.streams.push(stream);
                console.info($ctrl.streams)
            }
        }],
        templateUrl:'./modules/pages/stream-list/stream-list.html'
    });