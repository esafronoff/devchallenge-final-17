angular.module('modules.pages')
    .service('streamService',['$q','$http','AppConfig',function($q,$http,AppConfig){
        return {

            get: function(){
                let url = `${AppConfig.apiUrl}/streams/aggregate/all`;
                return $http.get(url);
            },

            newStream: function(data){
                var url = `${AppConfig.apiUrl}/streams`;
                return $http.post(url, data)
            },

            sendMessage: function(message){
                var url = `${AppConfig.apiUrl}/messages`;
                return $http.post(url, message)
            },
            sendEvent: function(event){
                var url = `${AppConfig.apiUrl}/events`;
                event.type = 'EVENT';
                return $http.post(url, event)
            }
            
        }
    }]);