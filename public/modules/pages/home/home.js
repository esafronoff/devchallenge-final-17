class HomePageController {
    constructor(userService, $state) {
        this.userService = userService;
        this.$state = $state;
    }
    logout() {
        this.userService.setCurrentUser(null);
        this.$state.go('home');
    }
}


angular.module('modules.pages')
    .component('homePage',{
        controller: HomePageController,
        templateUrl:'./modules/pages/home/home.html',
        controllerAs: 'vm'
    });