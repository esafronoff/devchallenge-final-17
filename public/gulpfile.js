var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var csso = require('gulp-csso');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var babel = require('gulp-babel');

gulp.task('sass', function() {
    gulp.src([
        'node_modules/angular-material/angular-material.min.css',
        'modules/**/*.scss',
        'components/**/*.scss'
    ])
        .pipe(concat('style.js'))
        .pipe(plumber())
        .pipe(sass())
        .pipe(gulp.dest('build'));
});

gulp.task('compress', function() {
    gulp.src([
        'modules/index.js',
        'components/**/*.js',
        'modules/**/*.js',
        'app.js'
    ])
        .pipe(concat('app.min.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        //.pipe(uglify())
        .pipe(gulp.dest('build'));

    gulp.src([
            'node_modules/angular/angular.js',
            'node_modules/angular-ui-router/release/angular-ui-router.js',
            'node_modules/angular-aria/angular-aria.min.js',
            'node_modules/angular-animate/angular-animate.js',
            'node_modules/angular-material/angular-material.min.js',
            'node_modules/angular-cookies/angular-cookies.js'
        ])
        .pipe(concat('lib.min.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('build'));
});

gulp.task('watch', function() {
    gulp.watch('modules/**/*.scss', ['sass']);
    gulp.watch('components/**/*.scss', ['sass']);
    gulp.watch('modules/**/*.js', ['compress']);
    gulp.watch('components/**/*.js', ['compress']);
});

gulp.task('default', ['sass', 'compress', 'watch']);