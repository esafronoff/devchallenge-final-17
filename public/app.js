angular.module('angularApp', ['ui.router','ngAnimate','ngAria','ngMaterial','modules', 'ngCookies'])
    .config(['$locationProvider','$stateProvider', function($locationProvider,$stateProvider) {
        $locationProvider.html5Mode(true);

        $stateProvider
            .state('home', {
                url: '/',
                template: '<home-page></home-page>',
                controller: ['$state','userService',function($state,userService){
                    if(userService.getCurrentUser()!=null)
                        $state.go('stream');
                }]
            })
            .state('stream', {
                url: '/stream',
                template: '<stream-list-page></stream-list-page>',
                controller: ['$state','userService',function($state,userService){
                    if(userService.getCurrentUser()==null)
                        $state.go('home');
                }]
            });

    }])
    .constant('AppConfig', {
        apiUrl: `${window.location.origin}/api`
    });